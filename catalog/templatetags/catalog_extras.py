from django import template
from catalog.models import Catalog

register = template.Library()

@register.inclusion_tag('catalog/menu.html')
def catalog_menu():
    catalogs = Catalog.objects.all()
    return {'catalogs':catalogs}

