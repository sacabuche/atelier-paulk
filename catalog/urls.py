from django.conf.urls.defaults import *
from django.contrib import admin
from catalog.views import CatalogDetailView, AtelierView


admin.autodiscover()

urlpatterns = patterns('core.views',
    # Example:
    url(r'^atelier/$', AtelierView.as_view(), name='catalog_atelier',
        kwargs={'slug':'atelier'}),
    url(r'^(?P<slug>[-\w]+)/$', CatalogDetailView.as_view(), name='catalog'),
)
