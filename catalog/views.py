#from django.shortcuts import render_to_response
#from django.template import RequestContext
from catalog.models import Catalog
from django.views.generic import DetailView

class CatalogDetailView(DetailView):

    context_object_name = "catalog"
    queryset = Catalog.objects.all()
    template_name = "catalog/catalog.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CatalogDetailView, self)\
                .get_context_data(*args, **kwargs)

        context['background'] = self.object.background
        context['images'] = self.object.images.all()
        return context

class AtelierView(CatalogDetailView):
    template_name = "catalog/atelier.html"

