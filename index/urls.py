from django.conf.urls.defaults import *
from index.views import IndexView


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index',
        kwargs={'slug':'accueil'}
    ),
)
