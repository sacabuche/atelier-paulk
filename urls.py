from django.conf.urls.defaults import *
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    (r'^', include('index.urls')),
    (r'^\+', include('banner.urls')),
    (r'^catalog/', include('catalog.urls')),
    (r'^contact/', include('contact_form.urls')),
    (r'^devis/', include('quote_form.urls')),
    (r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns +=  (url(r'^rosetta/', include('rosetta.urls')),)
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
