from django.contrib.sites.models import Site

def get_current_path(request):
    current_site = Site.objects.get_current().domain
    path = 'http://%s%s'%(current_site, request.get_full_path())
    return {
       'current_path': path,
     }
